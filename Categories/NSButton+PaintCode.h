//
//  NSButton+PaintCode.h
//  LookUp
//
//  Created by Grattan Johnny on 1/10/15.
//
//

#import <Cocoa/Cocoa.h>

@interface NSButton (PaintCode)

-(void)styleForPaintCode;

@end
