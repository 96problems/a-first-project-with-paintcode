//
//  NSButton+PaintCode.m
//  LookUp
//
//  Created by Grattan Johnny on 1/10/15.
//
//

#import "NSButton+PaintCode.h"

@implementation NSButton (PaintCode)

-(void)styleForPaintCode{
    [self setBordered:NO];
    [self setImagePosition:NSImageOnly];
    [self setButtonType:NSMomentaryChangeButton];
    [self setBezelStyle:NSRegularSquareBezelStyle];
}

@end
